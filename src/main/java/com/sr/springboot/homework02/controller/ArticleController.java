package com.sr.springboot.homework02.controller;


import com.sr.springboot.homework02.model.Article;
import com.sr.springboot.homework02.service.ArticleService;
import com.sr.springboot.homework02.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
public class ArticleController {

    private ArticleService articleService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    public void setArticleService(ArticleService articleService){
        this.articleService = articleService;
    }

    @GetMapping("/")
    public String index(Model model){
        List<Article> articles = articleService.findAllArticles();
        model.addAttribute("articles", articles);
        return "index";
    }

    @GetMapping("/create")
    public String create(Model model){
        model.addAttribute("article", new Article());
        return "article/create";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute Article article, @RequestParam MultipartFile file){
        String imageURL = fileStorageService.saveFile(file);
        article.setImageURL(imageURL);
        articleService.createArticle(article);
        return "redirect:/";
    }

    @GetMapping("/{id}/view")
    public String view(Model model, @PathVariable int id){
        Article article = articleService.findArticleById(id);
        model.addAttribute("article", article);
        return "article/view";
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable int id){
        articleService.deleteArticle(id);
        return "redirect:/";
    }


    @GetMapping("/{id}/update")
    public String update(Model model, @PathVariable int id){
        Article article= articleService.findArticleById(id);
        model.addAttribute("article", article);
        return "article/update";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute Article article, @RequestParam MultipartFile file){
        String imageURL = fileStorageService.saveFile(file);
        article.setImageURL(imageURL);
        articleService.createArticle(article);
        return "redirect:/";
    }

}
