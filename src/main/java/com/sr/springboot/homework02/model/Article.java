package com.sr.springboot.homework02.model;


import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Article {
    private int id;
    private String title;
    private String description;
    private String imageURL;

}
