package com.sr.springboot.homework02.service;


import com.sr.springboot.homework02.model.Article;

import java.util.List;

public interface ArticleService {
    List<Article> findAllArticles();
    Article findArticleById(int id);
    boolean createArticle(Article article);
    boolean updateArticle(int id, Article newArticle);
    boolean deleteArticle(int id);


}
