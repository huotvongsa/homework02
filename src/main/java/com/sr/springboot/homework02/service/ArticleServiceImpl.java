package com.sr.springboot.homework02.service;


import com.sr.springboot.homework02.model.Article;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    private List<Article> articles = new ArrayList<>(
            Arrays.asList(
                    new Article(1,"Heavenly Traveler Fate Record","A mortal has no choice but to enter the rivers and lakes to follow the tide, making constant efforts to control his own destiny. By chance, he enters the realm of comprehension and aims to pursue longevity"
                    ,"https://comrademao.com/wp-content/uploads/2021/05/206607.jpg" ),
                    new Article(2,"Against the Gods","Hunted for possessing a heaven-defying object, Yun Che is a young man in both that life and the next. Throwing himself off a cliff to spite his pursuers, Yun Che is reincarnated as Xiao Che, a recently poisoned teen in another realm. Just as hated in this life as the previous one, Che must overcome his own hostile clan, his own inability to cultivate, and his own frosty fiancée.\n" +
                            "\n"
                            ,"https://cdn.wuxiaworld.com/images/covers/atg.jpg?ver=b6377e1043744b345c0bdf83557f8b89a8018e94" ),
                    new Article(3,"Yama Rising","Yama Rising, also known as 'I Want To Be Yama' 我要做阎罗, is a Zongheng novel written by Nocturnal Stranger, 厄夜怪客. "
                            ,"https://cdn.wuxiaworld.com/images/covers/yr.jpg?ver=59f7ad3b7c7aa2ec4377844d5bb852338b47cef4" )
            )
    );

    @Override
    public List<Article> findAllArticles() {
        return articles;
    }

    @Override
    public Article findArticleById(int id) {
        for(Article article: articles){
            if(article.getId()== id ){
                return article;
            }
        }
        return null;
    }

    @Override
    public boolean createArticle(Article article) {
        return articles.add(article);
    }

    @Override
    public boolean updateArticle(int id, Article newArticle) {
        int index = -1;
        for(int i = 0 ;i<articles.size();i++){
            if(articles.get(i).getId()==id){
                index = i;
                break;
            }
        }
        articles.set(index, newArticle);
        return false;
    }

    @Override
    public boolean deleteArticle(int id) {
        int index = -1;
        for(int i = 0 ;i<articles.size();i++){
            if(articles.get(i).getId()==id){
                index = i;
                break;
            }
        }
        articles.remove(index);
        return true;
    }
}
