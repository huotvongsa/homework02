package com.sr.springboot.homework02.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileStorageServiceImp implements FileStorageService{

    private Path filestorageLocation;

    @Autowired
    public FileStorageServiceImp(@Value("${file.storage.server.path}") String path){
        filestorageLocation = Paths.get(path);
    };

    @Override
    public String saveFile(MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            if (fileName.contains("..")) {
                System.out.println("Error File Name");
                return null;
            }
            String[] fileParts = fileName.split("\\.");
            String extension = fileParts[1];
            fileName = UUID.randomUUID() + "." + extension;
            Path targetLocation = filestorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } finally {
            return null;
        }
    }

}
